#[allow(nonstandard_style, unused_variables)]
pub mod soap {
    include!(concat!(env!("OUT_DIR"), "/ilias.rs"));
}

use std::{
    borrow::Cow,
    collections::HashMap,
    fmt::{Debug, Display},
    str::FromStr,
};

use chrono::NaiveDateTime;
use html_escape::encode_text;
use roxmltree::Node;
use soap::{is_element, messages::*, types::*, SoapError};

type SoapResult<T> = Result<T, SoapError>;

pub struct Ilias {
    pub ilias: soap::services::ILIASSoapWebservice,
    pub sid: String,
    pub user_id: i32,
    pub base_url: &'static str,
}

const DATETIME_FORMAT: &str = "%F %T";

#[repr(i32)]
pub enum AttachmentMode {
    None = 0,
    Plain = 1,
    Zlib = 2,
    Gzip = 3,
}

#[derive(Debug)]
pub struct File {
    pub obj_id: String,
    pub title: String,
    pub description: Option<String>,
    pub filename: String,
    pub r#type: mime::Mime,
    pub size: usize,
    pub version: u32,
    pub max_version: u32,
    pub versions: Vec<FileVersion>,
}

impl File {
    fn parse(node: Node) -> Self {
        Self {
            obj_id: node.attribute("obj_id").unwrap().to_owned(),
            title: node
                .children()
                .find(is_element("Title"))
                .unwrap()
                .text()
                .unwrap()
                .to_owned(),
            description: node
                .children()
                .find(is_element("Description"))
                .unwrap()
                .text()
                .map(str::to_owned),
            filename: node
                .children()
                .find(is_element("Filename"))
                .unwrap()
                .text()
                .unwrap()
                .to_owned(),
            r#type: node.attribute("type").unwrap().parse().unwrap(),
            size: node.attribute("size").unwrap().parse().unwrap(),
            version: node.attribute("version").unwrap().parse().unwrap(),
            max_version: node.attribute("max_version").unwrap().parse().unwrap(),
            versions: node
                .children()
                .find(is_element("Versions"))
                .unwrap()
                .children()
                .map(FileVersion::parse)
                .collect(),
        }
    }
}

#[derive(Debug)]
pub struct FileVersion {
    pub version: u32,
    pub max_version: u32,
    pub date: NaiveDateTime,
    pub user_id: ObjectId,
    pub body: Vec<u8>,
}

impl FileVersion {
    fn parse(node: Node) -> Self {
        Self {
            version: node.attribute("version").unwrap().parse().unwrap(),
            max_version: node.attribute("max_version").unwrap().parse().unwrap(),
            date: NaiveDateTime::from_timestamp(
                node.attribute("date").unwrap().parse().unwrap(),
                0,
            ),
            user_id: ObjectId::parse(node.attribute("usr_id").unwrap())
                .expect_kind(ObjectType::User)
                .unwrap(),
            body: node
                .text()
                .map(base64::decode)
                .transpose()
                .unwrap()
                .unwrap_or_else(Vec::new),
        }
    }
}

impl Ilias {
    pub fn client(&self) -> &str {
        self.sid.split("::").nth(1).unwrap()
    }
    pub fn sessid(&self) -> &str {
        self.sid.split("::").nth(0).unwrap()
    }

    pub fn url() -> &'static str {
        soap::services::ILIASSoapWebservice::url()
    }

    pub async fn get_file(
        &self,
        ref_id: RefId,
        attachment_mode: AttachmentMode,
    ) -> SoapResult<File> {
        let xml = self
            .ilias
            .get_file_x_m_l(GetFileXMLRequest {
                sid: self.sid.clone(),
                ref_id: ref_id.0,
                attachment_mode: attachment_mode as i32,
            })
            .await?
            .filexml;

        let document = roxmltree::Document::parse(&xml).unwrap();

        Ok(File::parse(
            document.root().children().find(is_element("File")).unwrap(),
        ))
    }

    pub async fn get_web_link(&self, ref_id: RefId) -> SoapResult<()> {
        let res = self
            .ilias
            .read_web_link(ReadWebLinkRequest {
                sid: self.sid.clone(),
                ref_id: ref_id.0,
            })
            .await?;

        dbg!(res);

        todo!()
    }

    pub async fn get_exercise(
        &self,
        ref_id: RefId,
        attachment_mode: AttachmentMode,
    ) -> SoapResult<()> {
        let xml = self
            .ilias
            .get_exercise_x_m_l(GetExerciseXMLRequest {
                sid: self.sid.clone(),
                ref_id: ref_id.0,
                attachment_mode: attachment_mode as i32,
            })
            .await?
            .exercisexml;

        println!("xml: {xml}");

        let document = roxmltree::Document::parse(&xml).unwrap();

        dbg!(&document);

        Ok(())
    }

    pub async fn get_courses(&self, status: GetCoursesStatus) -> SoapResult<Vec<CourseInfo>> {
        let parameters = XmlResultSet::new(vec![maplit::hashmap! {
            "status" => status.bits().to_string(),
            "user_id" => self.user_id.to_string(),
        }]);

        let result: XmlResultSet = self
            .ilias
            .get_courses_for_user(GetCoursesForUserRequest {
                sid: self.sid.clone(),
                parameters: parameters.to_string(),
            })
            .await
            .unwrap()
            .xml
            .parse()
            .unwrap();

        let courses: Vec<_> = result
            .rows()
            .into_iter()
            .map(|row| CourseInfo {
                ref_id: RefId(row.get("ref_id").unwrap().parse().unwrap()),
                parent_ref_id: RefId(row.get("parent_ref_id").unwrap().parse().unwrap()),
                course: Course::parse(
                    roxmltree::Document::parse(row.get("xml").unwrap())
                        .unwrap()
                        .root()
                        .children()
                        .find(is_element("Course"))
                        .unwrap(),
                ),
            })
            .collect();

        Ok(courses)
    }

    pub async fn get_tree_children(
        &self,
        ref_id: RefId,
        types: &[ObjectType],
    ) -> SoapResult<Vec<Object>> {
        let xml = self
            .ilias
            .get_tree_childs(GetTreeChildsRequest {
                sid: self.sid.clone(),
                user_id: self.user_id,
                ref_id: ref_id.0,
                types: StringArray(types.iter().map(|t| t.to_string()).collect()),
            })
            .await?
            .object_xml;

        let document = roxmltree::Document::parse(&xml).unwrap();
        let objects = document
            .root()
            .children()
            .find(is_element("Objects"))
            .unwrap()
            .children()
            .filter(is_element("Object"))
            .flat_map(|object| {
                let r#type = object
                    .attribute("type")
                    .unwrap()
                    .to_owned()
                    .parse()
                    .unwrap_or_else(|err| {
                        panic!(
                            "error parsing object {:#?}\nunkown object type: {:?}",
                            object, err
                        )
                    });

                object
                    .children()
                    .filter(is_element("References"))
                    .map(move |references| {
                        let can_access = match references.attribute("accessInfo").unwrap() {
                            "granted" => true,
                            "no_permission" => false,

                            x => panic!("unexpected 'accessInfo' value: {:?}", x),
                        };

                        Object {
                            ref_id: RefId(
                                references
                                    .attribute("ref_id")
                                    .unwrap()
                                    .to_owned()
                                    .parse()
                                    .unwrap(),
                            ),
                            can_access,
                            r#type,
                            object_id: ObjectId {
                                id: object
                                    .attribute("obj_id")
                                    .unwrap()
                                    .to_owned()
                                    .parse()
                                    .unwrap(),
                                kind: r#type,
                            },
                            title: object
                                .children()
                                .find(is_element("Title"))
                                .unwrap()
                                .text()
                                .unwrap()
                                .to_owned(),
                            description: object
                                .children()
                                .find(is_element("Description"))
                                .unwrap()
                                .text()
                                .map(str::to_owned),
                            owner: ObjectId {
                                kind: ObjectType::User,
                                id: object
                                    .children()
                                    .find(is_element("Owner"))
                                    .unwrap()
                                    .text()
                                    .unwrap()
                                    .to_owned()
                                    .parse()
                                    .unwrap(),
                            },
                            create_date: NaiveDateTime::parse_from_str(
                                object
                                    .children()
                                    .find(is_element("CreateDate"))
                                    .unwrap()
                                    .text()
                                    .unwrap(),
                                DATETIME_FORMAT,
                            )
                            .unwrap(),
                            last_update: NaiveDateTime::parse_from_str(
                                object
                                    .children()
                                    .find(is_element("LastUpdate"))
                                    .unwrap()
                                    .text()
                                    .unwrap(),
                                DATETIME_FORMAT,
                            )
                            .unwrap(),
                        }
                    })
            })
            .collect();

        Ok(objects)
    }
}

/*
<Object type="book" obj_id="4995357">
  <Title>Terminbuchung mündliche Prüfung</Title>
  <Description>Sie können sich über das Buchungstool für einen
  Termin zur möndlichen Prüfung anmelden. Diese sind von etwa 15
  Minuten Dauer und…</Description>
  <Owner>4227430</Owner>
  <CreateDate>2021-01-26 12:13:11</CreateDate>
  <LastUpdate>2021-02-05 17:59:22</LastUpdate>
  <ImportId />
  <References ref_id="3821617" parent_id="3523304" accessInfo="granted">
    <TimeTarget type="0">
      <Timing starting_time="1611660054" ending_time="1611660054"
      visibility="0" />
      <Suggestion starting_time="1611660054"
      ending_time="1611660054" changeable="0" />
    </TimeTarget>
    <Operation>visible</Operation>
    <Operation>read</Operation>
    <Path>
      <Element ref_id="1" type="root">Repository</Element>
      <Element ref_id="78623" type="cat">
      Veranstaltungen</Element>
      <Element ref_id="3522918" type="cat">Wintersemester 2020/21
      KLIPS</Element>
      <Element ref_id="3523169" type="cat">
      Mathematisch-Naturwissenschaftliche Fakultät</Element>
      <Element ref_id="3523173" type="cat">Fachgruppe
      Physik</Element>
      <Element ref_id="3523304" type="crs">[WS20/21]
      Mathematische Methoden</Element>
    </Path>
  </References>
</Object>
*/
#[derive(Debug)]
pub struct Object {
    pub title: String,
    pub r#type: ObjectType,
    pub description: Option<String>,
    pub ref_id: RefId,
    pub object_id: ObjectId,
    pub owner: ObjectId,
    pub create_date: NaiveDateTime,
    pub last_update: NaiveDateTime,
    pub can_access: bool,
}

bitflags::bitflags! {
    pub struct GetCoursesStatus: u8 {
        const MEMBER = 1;
        const TUTOR = 2;
        const ADMIN = 4;
        const OWNER = 8;
    }
}

#[derive(Debug)]
pub struct CourseInfo {
    pub ref_id: RefId,
    pub parent_ref_id: RefId,
    pub course: Course,
}

// TODO: remaining variants
// 'cat','dbk','crs','fold','frm','grp','lm','sahs','glo','mep','htlm','exc','
// file','qpl','tst','svy','spl',
// 'chat','webr','mcst','sess','pg','st','wiki','book', 'copa'
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[non_exhaustive]
pub enum ObjectType {
    Course,
    Folder,
    Category,
    File,
    OpenCast,
    Book,
    User,
    Forum,
    Group,
    ItemGroup,
    MediaCast,
    Excercise,
    Session,
    WebReference,
}

impl FromStr for ObjectType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use ObjectType::*;
        Ok(match s {
            "crs" => Course,
            "cat" => Category,
            "file" => File,
            "fold" => Folder,
            "xoct" => OpenCast,
            "book" => Book,
            "usr" => User,
            "frm" => Forum,
            "grp" => Group,
            "itgr" => ItemGroup,
            "mcst" => MediaCast,
            "exc" => Excercise,
            "sess" => Session,
            "webr" => WebReference,
            _ => return Err(s.to_owned()),
        })
    }
}

impl Display for ObjectType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use ObjectType::*;
        write!(
            f,
            "{}",
            match self {
                Course => "crs",
                Folder => "fold",
                Category => "cat",
                File => "file",
                OpenCast => "xoct",
                Book => "book",
                User => "usr",
                Forum => "frm",
                Group => "grp",
                ItemGroup => "itgr",
                MediaCast => "mcst",
                Excercise => "exc",
                Session => "sess",
                WebReference => "webr",
            }
        )
    }
}

impl Ilias {
    pub async fn login(
        username: impl Into<Cow<'_, str>>,
        password: impl Into<Cow<'_, str>>,
    ) -> SoapResult<Self> {
        let ilias = soap::services::ILIASSoapWebservice::new();

        let mut this = Self {
            ilias,
            sid: String::new(),
            user_id: 0,
            base_url: Ilias::url()
                .strip_suffix("webservice/soap/server.php")
                .unwrap(),
        };

        let info = this.get_installation_info().await?;

        this.login_with_client(username, password, info.default_client)
            .await?;

        this.user_id = this.get_user_id().await?;

        Ok(this)
    }

    pub async fn get_user_id(&self) -> SoapResult<i32> {
        Ok(self
            .ilias
            .get_user_id_by_sid(GetUserIdBySidRequest {
                sid: self.sid.clone(),
            })
            .await?
            .usr_id)
    }

    pub async fn login_with_client(
        &mut self,
        username: impl Into<Cow<'_, str>>,
        password: impl Into<Cow<'_, str>>,
        client: impl Into<Cow<'_, str>>,
    ) -> SoapResult<()> {
        let username: Cow<str> = username.into();
        let password: Cow<str> = password.into();
        let client: Cow<str> = client.into();

        let sid = self
            .ilias
            .login(LoginRequest {
                username: username.into_owned(),
                password: password.into_owned(),
                client: client.into_owned(),
            })
            .await?
            .sid;

        self.sid = sid;

        Ok(())
    }

    pub async fn get_installation_info(&self) -> SoapResult<InstallationInfo> {
        let info = self
            .ilias
            .get_installation_info_x_m_l(GetInstallationInfoXMLRequest)
            .await?
            .xml;

        let info = roxmltree::Document::parse(&info).unwrap();

        Ok(InstallationInfo::parse(
            info.root()
                .children()
                .find(is_element("Installation"))
                .unwrap(),
        ))
    }

    async fn logout(&mut self) -> SoapResult<bool> {
        Ok(self
            .ilias
            .logout(LogoutRequest {
                sid: std::mem::take(&mut self.sid),
            })
            .await?
            .success)
    }
}

impl Drop for Ilias {
    fn drop(&mut self) {
        if !self.sid.is_empty() {
            let success = futures::executor::block_on(self.logout()).unwrap();

            assert!(success, "Failed to log out");
        }
    }
}

fn parse_bool(x: &str) -> bool {
    x.parse::<bool>()
        .ok()
        .or_else(|| match x.to_lowercase().as_str() {
            "true" | "yes" => Some(true),
            "false" | "no" => Some(false),
            _ => None,
        })
        .unwrap()
}

#[derive(Debug)]
pub enum SortDirection {
    Acending,
    Descending,
}

#[derive(Debug)]
pub struct Sort {
    pub direction: SortDirection,
    pub by: String,
}
impl Sort {
    fn parse(node: Node) -> Self {
        Self {
            by: node.attribute("type").unwrap().to_owned(),
            direction: match node.attribute("direction").unwrap().to_uppercase().as_str() {
                "ASC" => SortDirection::Acending,
                "DESC" => SortDirection::Descending,
                _ => panic!(),
            },
        }
    }
}

#[derive(Debug)]
pub struct MetaData {
    pub title: String,
    pub description: String,
    pub language: String,
    pub identifier: Identifier,
    pub keyword: String,
}

#[derive(Debug)]
pub struct Identifier {
    pub catalog: String,
    pub entry: String,
}

impl MetaData {
    fn parse(node: Node) -> Self {
        let general = node.children().find(is_element("General")).unwrap();

        assert_eq!(general.attribute("Structure"), Some("Hierarchical"));

        let identifier = general.children().find(is_element("Identifier")).unwrap();
        let title = general.children().find(is_element("Title")).unwrap();
        let language = general.children().find(is_element("Language")).unwrap();
        let description = general.children().find(is_element("Description")).unwrap();
        let keyword = general.children().find(is_element("Keyword")).unwrap();

        Self {
            identifier: Identifier {
                catalog: identifier.attribute("Catalog").unwrap().trim().to_owned(),
                entry: identifier.attribute("Entry").unwrap().trim().to_owned(),
            },
            title: title.text().unwrap().trim().to_owned(),
            language: language.attribute("Language").unwrap().trim().to_owned(),
            description: description.text().unwrap().trim().to_owned(),
            keyword: keyword.text().unwrap().trim().to_owned(),
        }
    }
}

#[derive(Debug)]
pub struct Course {
    pub id: ObjectId,
    pub meta_data: MetaData,
    pub sort: Sort,
    pub show_members: bool,
    // TODO: parse remaining fields?
}

impl Course {
    fn parse(node: Node) -> Self {
        let export_version: usize = node.attribute("exportVersion").unwrap().parse().unwrap();

        assert_eq!(export_version, 2, "Unexpected `exportVersion`");

        let id = ObjectId::parse(node.attribute("id").unwrap())
            .expect_kind(ObjectType::Course)
            .unwrap();

        let show_members = node
            .attribute("showMembers")
            .map(parse_bool)
            .unwrap_or(false);

        let sort = Sort::parse(node.children().find(is_element("Sort")).unwrap());
        let meta_data = MetaData::parse(node.children().find(is_element("MetaData")).unwrap());

        Self {
            id,
            sort,
            meta_data,
            show_members,
        }
    }
}

#[derive(Debug)]
pub struct InstallationInfo {
    pub default_client: String,
    pub clients: Vec<ClientInfo>,
}

impl InstallationInfo {
    fn parse(installation: Node) -> Self {
        let settings = installation
            .children()
            .find(is_element("Settings"))
            .unwrap();

        let default_client = settings
            .children()
            .filter(is_element("Setting"))
            .find(|node| node.attribute("key") == Some("default_client"))
            .unwrap()
            .text()
            .unwrap()
            .to_owned();

        let clients = installation.children().find(is_element("Clients")).unwrap();

        let clients = clients
            .children()
            .filter(is_element("Client"))
            .map(ClientInfo::parse)
            .collect();

        Self {
            default_client,
            clients,
        }
    }
}

#[derive(Debug)]
pub struct ClientInfo {
    pub inst_id: u32,
    pub id: String,
    pub enabled: bool,
    pub default_lang: String,
}

impl ClientInfo {
    fn parse(node: Node) -> Self {
        Self {
            inst_id: node.attribute("inst_id").unwrap().parse().unwrap(),
            id: node.attribute("id").unwrap().to_owned(),
            enabled: parse_bool(node.attribute("enabled").unwrap()),
            default_lang: node.attribute("default_lang").unwrap().to_owned(),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct ObjectId {
    pub id: i32,
    pub kind: ObjectType,
}

impl ObjectId {
    fn parse(x: &str) -> ObjectId {
        let mut parts = x.split('_');
        assert_eq!(parts.next(), Some("il"));
        let _installation: i32 = parts.next().unwrap().parse().unwrap();
        let kind = parts.next().unwrap().parse().unwrap();
        let id = parts.next().unwrap().parse().unwrap();
        assert!(parts.next().is_none());

        ObjectId { id, kind }
    }

    fn expect_kind(self, kind: ObjectType) -> Option<Self> {
        if self.kind == kind {
            Some(self)
        } else {
            None
        }
    }
}

impl std::fmt::Debug for ObjectId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}({}_{})",
            std::any::type_name::<Self>().split(':').last().unwrap(),
            self.kind,
            self.id
        )
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct RefId(pub i32);

impl std::fmt::Debug for RefId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}({})",
            std::any::type_name::<Self>().split(':').last().unwrap(),
            self.0
        )
    }
}

struct XmlResultSet {
    colspecs: Vec<String>,
    rows: Vec<Vec<String>>,
}

impl Debug for XmlResultSet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.rows().fmt(f)
    }
}

impl XmlResultSet {
    fn new(in_rows: Vec<HashMap<&str, String>>) -> Self {
        let mut colspec = Vec::<String>::new();
        let mut rows = Vec::<Vec<String>>::new();

        for mut row in in_rows {
            let mut res_row = Vec::new();

            for key in &colspec {
                res_row.push(row.remove(key.as_str()).unwrap_or_else(String::new));
            }

            for (k, v) in row.into_iter() {
                // new colspecs

                colspec.push(k.to_owned());

                for row in &mut rows {
                    row.push(String::new());
                }

                res_row.push(v);
            }

            rows.push(res_row);
        }

        XmlResultSet {
            colspecs: colspec,
            rows,
        }
    }

    fn rows(&self) -> Vec<HashMap<String, String>> {
        let mut res = Vec::new();

        for row in &self.rows {
            let mut res_row = HashMap::new();

            for (i, key) in self.colspecs.iter().enumerate() {
                res_row.insert(key.clone(), row[i].clone());
            }

            res.push(res_row);
        }

        res
    }
}

impl Display for XmlResultSet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let colspecs = self
            .colspecs
            .iter()
            .map(|colspec| format!("<colspec name={:?} />", colspec))
            .collect::<String>();
        let rows = self
            .rows
            .iter()
            .map(|row| {
                format!(
                    "<row>{}</row>",
                    row.iter()
                        .map(|value| format!("<column>{}</column>", encode_text(value)))
                        .collect::<String>()
                )
            })
            .collect::<String>();

        write!(
            f,
            "<result><colspecs>{colspecs}</colspecs>{rows}</result>",
            colspecs = colspecs,
            rows = rows
        )
    }
}

/*
<result>
<colspecs>
    <colspec name="user_id" />
    <colspec name="status" />
</colspecs>
    <row>
        <column>{}</column>
        <column>{}</column>
    </row>
</result>
*/

impl FromStr for XmlResultSet {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let document = roxmltree::Document::parse(s).unwrap();

        let result = document
            .root()
            .children()
            .find(is_element("result"))
            .unwrap();

        let mut colspecs: Vec<(String, usize)> = result
            .children()
            .find(is_element("colspecs"))
            .unwrap()
            .children()
            .filter(is_element("colspec"))
            .enumerate()
            .map(|(i, colspec)| {
                (
                    colspec.attribute("name").unwrap().to_owned(),
                    colspec
                        .attribute("idx")
                        .map(|x| x.parse().unwrap())
                        .unwrap_or(i),
                )
            })
            .collect();

        colspecs.sort_by(|(_, a), (_, b)| a.cmp(b));

        let colspecs: Vec<String> = colspecs.into_iter().map(|(x, _)| x).collect();

        let rows = result.children().find(is_element("rows")).unwrap();

        let rows: Vec<Vec<String>> = rows
            .children()
            .filter(is_element("row"))
            .map(|row| {
                let mut row: Vec<String> = row
                    .children()
                    .filter(is_element("column"))
                    .map(|column| column.text().unwrap_or("").to_owned())
                    .collect();
                row.resize_with(colspecs.len(), String::new);
                row
            })
            .collect();

        Ok(Self { colspecs, rows })
    }
}

/*


/*
let res = ilias
    .get_tree_children(GetTreeChildsRequest {
        sid: sid.clone(),
        user_id,
        ref_id: 230736,
        types: StringArray(["cat"].iter().map(|&x| x.to_owned()).collect()),
    })
    .await
    .unwrap()
    .object_xml;

println!("{}", res);
*/

/*
let types = StringArray(["lm", "crs"].iter().map(|&x| x.to_owned()).collect());
let res = ilias
    .search_objects(SearchObjectsRequest {
        sid: sid.clone(),
        user_id,
        types,
        key: "".to_owned(),
        combination: "or".to_owned(),
    })
    .await
    .unwrap()
    .object_xml;

println!("{}", res);
*/

/*
let obj_ids = ilias
    .get_obj_ids_by_ref_ids(GetObjIdsByRefIdsRequest {
        sid: sid.clone(),
        ref_ids: IntArray(vec![1]),
    })
    .await
    .unwrap()
    .obj_ids
    .0;

dbg!(obj_ids);
*/

let status = 1;

let res = ilias
    .get_courses_for_user(GetCoursesForUserRequest {
        sid: sid.clone(),
        parameters: format!(
            r#"
            <result>
            <colspecs>
                <colspec name="user_id" />
                <colspec name="status" />
            </colspecs>
                <row>
                    <column>{}</column>
                    <column>{}</column>
                </row>
            </result>
            "#,
            user_id, status
        ),
    })
    .await
    .unwrap()
    .xml;

println!("{}", res);

/*
let res = ilias
    .get_course_x_m_l(GetCourseXMLRequest {
        sid: sid.clone(),
        course_id: 3862576,
    })
    .await
    .unwrap();

println!("{}", res.xml);
*/

/*
let res = ilias
    .get_object_by_reference(GetObjectByReferenceRequest {
        sid: sid.clone(),
        reference_id: ref_id,
        user_id,
    })
    .await
    .unwrap()
    .object_xml;

println!("{}", res);
*/

/*
let res = ilias
    .get_ref_ids_by_obj_id(GetRefIdsByObjIdRequest {
        sid: sid.clone(),
        obj_id: "5182873".to_owned(), // 5047704, 5209685, 5182873, 5216113
    })
    .await
    .unwrap();

eprintln!("{:?}", res);

let ref_id = *res.ref_ids.0.first().unwrap();
*/
// let ref_id = 3949887;

/*
let res = ilias
    .get_operations(GetOperationsRequest { sid: sid.clone() })
    .await
    .unwrap();

println!("{:#?}", res);

let res = ilias
    .get_object_tree_operations(GetObjectTreeOperationsRequest {
        sid: sid.clone(),
        user_id,
        ref_id,
    })
    .await
    .unwrap();

println!("{:#?}", res);
*/

/*
let res = ilias
    .get_user_x_m_l(GetUserXMLRequest {
        sid: sid.clone(),
        user_ids: IntArray(vec![user_id]),
        attach_roles: 1,
    })
    .await
    .unwrap()
    .xml;

println!("{}", res);
*/

/*
let res = ilias
    .get_object_by_reference(GetObjectByReferenceRequest {
        sid: sid.clone(),
        reference_id: ref_id,
        user_id,
    })
    .await
    .unwrap()
    .object_xml;

println!("{}", res);
*/

/*
let res = ilias
    .get_tree_children(GetTreeChildsRequest {
        sid: sid.clone(),
        user_id,
        ref_id,
        types: StringArray(
            [/*"xoct", "fold" */]
                .iter()
                .copied()
                .map(|x: &str| x.to_owned())
                .collect(),
        ),
    })
    .await
    .unwrap()
    .object_xml;

println!("{}", res);
*/

/*
let res = ilias
    .get_path_for_ref_id(GetPathForRefIdRequest {
        sid: sid.clone(),
        ref_id,
    })
    .await
    .unwrap()
    .xml;

eprintln!("{}", res);
*/

ilias.logout(LogoutRequest { sid }).await.unwrap();

Ok(())
*/
