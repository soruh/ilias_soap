use soap::{
    generator::{Generator, State},
    parser::WsdlFile,
};
use std::{env, fs::File, io::Write, path::PathBuf};

fn main() {
    use std::io::Read;

    let mut input = String::new();
    std::fs::File::open("assets/ilias.wsdl")
        .unwrap()
        .read_to_string(&mut input)
        .unwrap();

    let wsdl = WsdlFile::parse(&input);

    // println!("{:#?}", &wsdl);

    let generator = Generator::new(wsdl);
    let mut state = State::new();

    let res = generator.generate(&mut state);

    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

    File::create(out_dir.join("ilias.rs"))
        .unwrap()
        .write_all(res.to_string().as_bytes())
        .unwrap();
}
